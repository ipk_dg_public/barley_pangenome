#!/bin/zsh

bbduk='/filer-dg/agruppen/DG/mascher/source/BBMap_37.93/bbduk.sh'

ref=$1
shift
out_dir=$1
shift
dir=$1

base=$dir:r:t


{
 find $dir | egrep 'fq.gz$|fastq.gz$|R1.fastq.gz$' | xargs gzip -cd \
  | $bbduk -Xmx100G -k=31 overwrite=t int=f ref=$ref in=stdin.fq out=/dev/null rpkm=/dev/stdout nzo=f \
  | grep -v '^#' | cut -f 1,5 | awk '$2 >= 1' | awk '{split($1, a,"_"); print a[1]"\t" $2}' \
  | awk '{a[$1]++; b[$1]+=$2;} END {for (i in a) print i,a[i],b[i]}' | gzip > $out_dir/${base}_count.tsv.gz
} 2> $out_dir/${base}_count.err
