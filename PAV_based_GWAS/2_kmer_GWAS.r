# The following R codes were tested in R version 3.5.1
source("http://zzlab.net/GAPIT/gapit_functions.txt")

# Read passport data
readRDS('/filer-dg/180507_passport_GBS_WGS_merged.Rds')->pp_wgs

#Load list of PAVs used for genotyping
fread("/filer-dg/agruppen/summary/Selected_SVs_for_genotyping_table.bed") -> pav
pav[, .(ref_chr, ref_start, ref_stop, size, type, v.id)] -> mdt

# Now read kmer mapping results
fread("find /filer-dg/agruppen/summary/PAV_genotype_core200 | grep 'count.tsv.gz$'",head=F, col.names="file")-> files
files[, accession := sub("_count.tsv.gz$", "", sub("^.*/PAV_genotype_core200/", "", file))]


# define function to read k-mer counts
read_kmer_count<-function(info, gt, cores=1){
 f <- info
 rbindlist(mclapply(mc.cores=cores, 1:nrow(files), function(i){
 n <- files[i, file]
 fread(header=F, paste("zcat", n))->z
 setnames(z, c("v.id", "mapped_kmers", "count"))
 z[, accession := files[i, accession]]
 z[gt, on="v.id"][, .(chr=ref_chr, v.id, type, size, mapped_kmers, count, accession)] -> c
 c[is.na(mapped_kmers), accession := files[i, accession]]
 c[is.na(mapped_kmers), mapped_kmers := 0]
 c[is.na(count), count := 0]
 })) 
}

# Now read
read_kmer_count(info=files, gt=mdt, cores=30) -> PAV_gt

# change into wide table of mapped kmer count
dcast(PAV_gt, chr + v.id + size + type ~ accession, value.var='mapped_kmers')->w

# Normalize mapped count to billion
options(digits = 2)
setdiff(names(w), c("chr", "v.id", "size", "type")) -> lst

for (i in lst) w[, (i) := w[[i]]/sum(w[[i]])*1000000000]

# Count row mean and st.deviation
w[, mean := rowMeans(.SD), .SDcols = lst] 
w[, sd := sd(.SD), by=v.id, .SDcols = lst]
w[, cutoff := (sd/mean)]

# filter variants that show optimum variation for GWAS
w[cutoff >= 0.3] -> gw
gw[, c("mean", "sd", "cutoff") := c(NULL, NULL, NULL)]

# Format wide format for GAPIT
mm <- gw

mm[, c("chr", "size", "type") := c(NULL, NULL, NULL)]


# convert the data.table to long format
melt(gw, id="v.id", measure=setdiff(names(gw), c("chr", "v.id", "size", "type")), variable.factor=F, variable.name="id", value.name="count")->z

#scale to interval 0:2 to make counts compatible with GAPIT
z[, x := 2*(count - min(count))/(max(count) - min(count)), by=v.id]

#cast to wide matrix for GAPIT: rows are samples, columns are markers
dcast(z, id ~ v.id, value.var='x')->q

# read phenotypic data as a table
rt <- readRDS("phenotype_data.Rds")

#format table for GAPIT, row are samples, columns are traits
dcast(rt, accession ~ trait, value.var='value')->rt
rt[, panel := "BRIDGE"]
rt[, taxa := paste0("BRIDGE_WGS_", accession)]

#make table with positional information for GAPIT
mm[, .(v.id)] -> vid
mdt[vid, on="v.id"][, .(ref_chr, ref_start, v.id)] -> a

a[, .(SNP=v.id, Chromosome=ref_chr, Position=ref_start)] -> gm
gm[, Chromosome:= sub("chr", "", Chromosome)]
gm[, Chromosome:= sub("H", "", Chromosome)]
gm[Chromosome == "Un",Chromosome:= 0]


#matrix for grain hull
rt[, .(taxa, grain_hull)][q[, .(taxa=id)], on='taxa'][, idx := 1:.N]->Y

#matrix for row rtpe
rt[, .(taxa, convar)][q[, .(taxa=id)], on='taxa']->Y2
Y2[!convar %in% c(1,5), convar := NA] # select only two and six-rowed types
Y2[, .N, key=convar]


# matrix for annual growth habit (taken from passport data)
pp_wgs[q$id, on='sample.id'][, .(taxa=sample.id, annual_growth_habit)]->Y3
Y3[!annual_growth_habit %in% c("spring", "winter"), annual_growth_habit := NA]
Y3[, annual_growth_habit := as.integer(ifelse(annual_growth_habit == "winter", 0, 1))]

#matrix for rachilla hairs
rt[, .(taxa, rachilla_hairs)][q[, .(taxa=id)], on='taxa']->Y4
Y4[!rachilla_hairs %in% c(1,2), rachilla_hairs := NA]
Y4[, .N, key=rachilla_hairs]

#matrix for awn roughness
rt[, .(taxa, awns_roughness)][q[, .(taxa=id)], on='taxa']->Y5
Y5[!awns_roughness %in% c(1,2), awns_roughness := NA]
Y5[, .N, key=awns_roughness]


#merge tables and sort by id to ensure same order as in genotype table (GAPIT use the order of rows, not the row names)
Y2[Y, on="taxa"]->y
Y3[y, on="taxa"]->y
Y4[y, on="taxa"]->y
Y5[y, on="taxa"]->y
data.frame(y[order(idx)][, idx := NULL])->YY


#load kinship form prior runs (of course not available when you run it for the first time)
#data.frame(fread('/filer-dg/agruppen/gapit_181207/GAPIT.Kin.VanRaden.csv', head=F))->ki
#colnames(ki) <- c('taxa', ki[, 1])
#res_gapit <- GAPIT(Y=data.frame(YY), KI=data.frame(ki), GD=data.frame(q), GM=gm)

# GAPIT write to current working directory, so change wd to set output directory and change it back afterwards (First time)
setwd('/filer-dg/agruppen/dg2/gapit_181207')

# omit KI argument to compute kinship matrix for the first time
res_gapit <- GAPIT(Y=data.frame(YY), GD=data.frame(q), GM=gm)

setwd('/filer-dg/agruppen/dg2/mascher/pangenome_gbs_mapping_181206')

# check out result directory for all the results
