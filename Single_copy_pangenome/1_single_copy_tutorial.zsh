#working directory: /filer-dg/agruppen/dg3/jayakodi/single_copy_pangenome/contig_assembly

list='contigs_hor_list_36.txt' # list of assemblies (name, path to fasta) 
mincount=2 # mask k-mers with frequency >= $mincount
prefix='pangenome_191114' # output directory
mask='./mask_assembly.zsh' # path to script

#mask and create symlinks
while read name fa; do
  out=$prefix/assemblies/$name
  $mask --fasta $fa --mincount $mincount --out $out
  ln -s $fa $out/reference.fasta
  ln -s $fa.fai $out/reference.fasta.fai
  echo $name > $out/name.txt
done < $list &

#extract single copy sequences with a minimum length of 100 bp, regions separated by 50 bp or less will be merged
prefix='pangenome_191114'
find "$prefix/assemblies" -type d | grep -v "^$prefix/assemblies$" | while read i; do
 ./get_single_copy.zsh --dir $i --minlen 100 --dist 50 &
done

#define paths
prefix='pangenome_191114'
bedtools='/opt/Bio/bedtools/2.26.0/bin/bedtools'
samtools='/opt/Bio/samtools/1.9/bin/samtools'
n50='/filer-dg/agruppen/seq_shared/mascher/code_repositories/triticeae.bitbucket.io/shell/n50'
formatdb='/opt/Bio/blast+/2.2.30/bin/legacy_blast.pl formatdb --path /opt/Bio/blast+/2.2.30/bin/'
input=$prefix/'191118_all_assemblies_occ1_d50_l100.fasta' #combined FASTA of masked assemblies

#make FASTA of masked input assemblies
find -L $prefix | grep '_noGaps_l100_d50.fasta$' | sort | xargs cat > $input

# Get 20 pangenome sequences
less single_copy_pan20.txt | xargs cat > pan_20_sc_seqs.fasta
cat pan_20_sc_seqs.fasta pangenome_191114/191118_all_assemblies_occ1_d50_l100.fasta > temp.fa
mv temp.fa 191118_all_assemblies_occ1_d50_l100.fasta


{ $samtools faidx $input && $n50 $input.fai > $input.n50 
 eval $formatdb -p F -i $input 
} & 

#parallel BLAST all vs. all
prefix='pangenome_191114'
input=$prefix/'191118_all_assemblies_occ1_d50_l100.fasta'
ref=$input
query=$input
outdir=$prefix/"${input:t:r}_self_blast"
list=${input:r}_bed_files.txt

#split query
mkdir -p $outdir
awk '{print $1"\t"0"\t"$2}' $query.fai > $query.bed

#the number of lines 16322 is chosen to yield ~ 1000 parts, more small parts are better
split -u --numeric-suffixes=1 -a 4 -l 16322 --additional-suffix=".bed" $query.bed $outdir/${outdir:t}_ 

find $outdir -type f | grep 'bed$' > $list

#run parallel blast, don't use too many threads as RAM can be issue
list='/filer-dg/agruppen/dg3/jayakodi/single_copy_pangenome/contig_assembly/pangenome_191114/191118_all_assemblies_occ1_d50_l100_bed_files.txt'
ref='/filer-dg/agruppen/dg3/jayakodi/single_copy_pangenome/contig_assembly/pangenome_191114/191118_all_assemblies_occ1_d50_l100.fasta'
query='/filer-dg/agruppen/dg3/jayakodi/single_copy_pangenome/contig_assembly/pangenome_191114/191118_all_assemblies_occ1_d50_l100.fasta'
threads=10

cat $list| parallel --will-cite -j $threads ./blast_wrap.zsh $ref $query '{}' 80 100 &

#proceed in R

#extract cluster representatives
input='/filer-dg/agruppen/dg3/mascher/single_copy_pangenome_190826/pangenome_190826/191016_all_assemblies_occ1_d50_l100.fasta'
replist='/filer-dg/agruppen/dg3/mascher/single_copy_pangenome_190826/pangenome_190826/191016_all_assemblies_occ1_d50_l100.rep_len.txt'
rep=${input:r}.rep.fasta
{
 samtools faidx -r $replist $input | cut -d : -f -2 > $rep #bedtools/parallel are too slow if the number of sequences is large, hence faidx -r
 samtools faidx ${rep} 
 formatdb -p F -i ${rep} 
} &
