library(igraph) #for graph algorithms
# check for bitbucket scripts https://tritexassembly.bitbucket.io
source("$bitbucket/R/pseudomolecule_construction.R") #for n50() function

#read BLAST results and write to tsv fileG
d <- '/filer-dg/agruppen/dg3/mascher/single_copy_pangenome_190826/pangenome_190826/191016_all_assemblies_occ1_d50_l100_self_blast'
fread(cmd=paste("find", d, "| grep 'tsv.gz$'"), head=F)$V1->f
rbindlist(mclapply(f, mc.cores=20, function(i){
 fread(cmd=paste("zcat", i, "| cut -f -10"), head=F) -> z
 setnames(z, c("seq1", "seq2", "percent_identity", "alignment_length", "mismatches", "gaps",
	    "seq1_start", "seq1_end", "seq2_start", "seq2_end"))
 z[, orientation := ifelse(seq2_start <= seq2_end, 1, -1)]
 z[orientation == -1, c("seq2_start", "seq2_end") := list(seq2_end, seq2_start)]
 z[seq1 != seq2]->z
})) -> z
z[, genotype1 := sub("\\|.*", "", seq1)]
setorder(z, genotype1, orientation)
fwrite(z, sep="\t", file=paste0(d, ".tsv"), scipen=20, nThread=20)
z -> blast

#read info on input sequences
f <- '/filer-dg/agruppen/dg3/mascher/single_copy_pangenome_190826/pangenome_190826/191016_all_assemblies_occ1_d50_l100.fasta.fai'
fread(f, sel=1:2, col.names=c('seq', 'seq.length'))->info
info[, genotype := sub("\\|.*", "", seq)]
info[, chr := sub(":.*", "", sub("^.*\\|", "", seq))]
info[, start := as.integer(sub("-.*", "", sub("^.*:", "", seq)))]
info[, end := as.integer(sub(".*-", "", seq))]
setkey(info, seq)

#construct overlap graph, minimum identity 90 %, minimum alignment length 100 bp
p <- 90
a <- 100
unique(blast[percent_identity >= p & alignment_length >= a][, .(seq1, seq2)])->e
graph.edgelist(as.matrix(e[, .(seq1, seq2)]), directed=F)->g
g + vertex(info[!c(e$seq1, e$seq2)]$seq) -> g

#analyze graph

options(scipen=20) #needed to ensure proper formating of numbers
data.table(seq=V(g)$name, cluster=paste0("cluster_", clusters(g)$membership))->mem
info[mem, on='seq']->mem
#the cluster representative is the longest sequence
setorder(mem, -seq.length)
mem[, cluster.idx := 1:.N, by=cluster]
mem[, rep := cluster.idx == 1]

mem[, .(size=.N), key=cluster] -> cl
mem[rep == T, .(cluster, rep=seq, rep.length=seq.length)][cl, on='cluster']->cl
mem[, .N, key=.(cluster, genotype)][, .(ngenotype=.N), key=cluster][cl, on='cluster']->cl

#write list of cluster representatives
fwrite(cl[, .(rep)], col.names=F, file=paste0(sub(".fasta.fai$", "", f), ".rep.txt"))

#assign clusters to chromosomes
mem[, .N, key=.(chr, genotype, cluster)][order(-N)][, c("idx", "pchr") := list(.GRP, N/sum(N)), by=.(genotype, cluster)][]->x

x[!duplicated(idx)][, .(chr, pchr, idx, cluster, genotype)] -> y
x[chr != 'chrUn', .(nchr=.N), key=idx][y, on='idx']->y
mem[y[, .(chr, genotype, cluster)], on=c('chr', 'genotype', 'cluster')]->w
y[w[, .(start=min(start), end=max(end)), key=.(cluster, genotype)], on=c('cluster', 'genotype')]->yy
yy[, length := 1 + end - start]
yy[mem[, .(nseq=.N), key=.(genotype, cluster)], on=c('genotype', 'cluster')]->yy

yy[, .(max.length = max(length), min.length=min(length), mean.length=mean(length), 
       sd.length=sd(length)), key=.(cluster)]->ww
ww[, cv.length := sd.length / mean.length]
y[chr != 'chrUn', .N, key=.(cluster, chr)][, .(nchr=.N), key=.(cluster)][ww, on='cluster']->ww
ww[cl, on='cluster']->cl
mem[, .(rep=seq, rep.genotype=genotype)][cl, on='rep']->cl

setkey(cl, cluster) #information per cluster
setkey(mem, seq) #membership, assignment input sequence to cluster
setkey(yy, cluster, genotype) #assignment of genotypes to cluster

# Note that multiple single-copy sequences from the same genotypes can be in cluster, for example a 1000 bp single-copy sequence in Barke can correspond to two smaller sequence in Barke with a between them

#save RDS file
cluster_info=list(mem=mem, info=cl, info_genotype=yy)
saveRDS(file=paste0(sub(x=f, ".fasta.fai", ""), "_cluster_info.Rds"), cluster_info)

## sanity checks

#longest sequence contributed by a single genotype to a cluster
setorder(cluster_info$mem[, .(l=sum(seq.length)), key=.(cluster, genotype)], -l)[1]
#1: cluster_224591   OUN333 52298
#largest no. of sequence contributed to cluster by a single genotype
setorder(cluster_info$mem[, .(l=sum(seq.length)), key=.(cluster, genotype)], -n)[1]
#1: cluster_32372 HOR_13821 35
#largest avg contribution
setorder(cluster_info$mem[, .(l=sum(seq.length)), key=cluster][cluster_info$info, on='cluster'][, l := l/ngenotype], -l)[1]
#1: cluster_467294 26269.9 RGT_Planet|chr4H:4691588-4705811   RGT_Planet    1
#check chromosome assignment
cluster_info$info[, .N, key=nchr]
#   nchr       N
#1:   NA    7854
#2:    1 1459489
#3:    2    4756
#4:    3     351
#5:    4      52
#6:    5       6

#get summary table
cluster_info$mem[genotype == 'Morex'][order(seq.length)][!duplicated(cluster)][, .(cluster, chr, start, end, seq.length, seq)]->z0
z0[cluster_info$info[, .(cluster, rep, rep.genotype, rep.length, cv.length, ngenotype)], on='cluster']->ze

cluster_info$mem[ze$cluster, on='cluster']->im
im[, .(l=round(sum(seq.length)/1e6,1), n=.N, n50=n50(seq.length)), key=.(genotype=paste(genotype, "single-copy"))]->x
x[, .(mean(l), sd(l), mean(n), mean(n50))]->xx

rbind(
 data.table(genotype = "single-copy sequence per genotype (mean, SD)",
	    l=paste0(round(xx$V1, 1), " Mb, ", round(xx$V2, 1), " Mb"),
	    n=round(mean(xx$V3), 0),
	    n50=round(mean(xx$V4), 0)
 ),
 ze[, .(genotype="all clusters", l=paste0(round(sum(rep.length)/1e6,1), " Mb"), n=.N, n50=n50(rep.length))],
 #clusters not present everywhere
 ze[ngenotype < 20, .(genotype="PAV clusters", l=paste0(round(sum(rep.length)/1e6, 1), " Mb"), n=.N, n50=n50(rep.length))],
 #clusters not present in only a single genotype
 ze[ngenotype == 1, .(genotype="singletons", l=paste0(round(sum(rep.length)/1e6, 1), "Mb"), n=.N, n50=n50(rep.length))]
 )->vv
vv[, n50 := paste0(formatC(n50, big.mark=","), " kb")]
vv[, n := formatC(n, format="d", big.mark=",")]
setnames(vv, c("", "size", "number of sequences", "N50"))
write.xlsx(vv, "191128_cluster_summary_pan20.xlsx")

## Extrace core and variables
#Core (402.6 Mb)
ze[ngenotype == 20] -> core
fwrite(core, file="191128_cluster_core_clusters.tsv", sep="\t", quote=F)

#Variable (176.9 Mb)
ze[ngenotype > 1 & ngenotype <20] -> vb
fwrite(vb, file="191128_cluster_variable_clusters.tsv", sep="\t", quote=F)

#unique (59.1 Mb)
ze[ngenotype == 1] -> uq
fwrite(uq, file="191128_cluster_unique_clusters.tsv", sep="\t", quote=F)

#get saturation curve, assemblies are sorted by amount of singleton sequence
cluster_info$info[ngenotype == 1][, .N, key=rep.genotype][order(N)]$rep.genotype -> x0

# saveRDS(x0, file="saturation_SC_pangenome_20.Rds")

c(rev(unlist(lapply(1:(length(x0)-1), function(i){
 print(i)
 sum(cluster_info$info[unique(cluster_info$mem[!x0[1:i], on="genotype"]$cluster), on="cluster"]$rep.length)
}))), cluster_info$info[, sum(rep.length)]) -> res

# Save
saveRDS(res, file="sorted_SC_pangenome_20.Rds")

#merge with passport data
readRDS('/filer-dg/agruppen/dg3/mascher/single_copy_pangenome_190826/pangenome_190826/190918_core_selection_for_pangenome_colpch_id.Rds') -> pp
data.table(id=x0, x=1:length(x0)) -> x
x[id == "FT11", id := "B1K-04-12"]
x[id == "Chiba", id := "Chiba_Damai"]
x[id == "OUN333", id := "OUN_333"]
x[id == "HHOR3365", id := "HOR_3365"]
pp[, .(id, country_of_origin, name, row_type, annual_growth_habit)][x, on="id"] -> x
x[id != "B1K-04-12", ll := paste0(name, ", ", sub("six-rowed", "6-rowed", row_type), ", ", annual_growth_habit)]
x[id == "B1K-04-12", ll := name]

#plot 
pdf("2020_single_copy_pangenome.pdf", height=6, width=15)
par(mar=c(5,23, 1, 1))
plot(res/1e6, length(res):1, pch=19, cex=1.7, ylimn=c(20,1), xlim=c(500, 650), axes=F, ylab="", xlab="", col="red")
title(xlab="single-copy pan-genome size (Mb)", cex.lab=1.7)
x[axis(2, x, ll, adj=1, las=1, font=2)]
axis(1, cex.axis=1.5)
dev.off()
