#!/bin/zsh

bedtools='/opt/Bio/bedtools/2.26.0/bin/bedtools'

m=(-dist 50)
l=(-minlen 100)

zparseopts -D -K -- -minlen:=l -dir:=d -dist:=m

dist=$m[2]
dir=$d[2]
minlen=$l[2]

read name < $dir/name.txt
find $dir | grep '_noGaps.bed$' | read ng
bed=${ng:r}_l${minlen}_d${dist}.bed

awk -v minlen=$minlen '$3-$2 >= minlen' $ng | bedtools merge -d $dist > $bed 

awk -v name=$name '{print $0"\t"name"|"$1":"$2+1"-"$3}' $bed \
 | $bedtools getfasta -bed - -fi $bed:h/reference.fasta -fo /dev/stdout -name \
 | sed 's/::.*$//' > ${bed:r}.fasta
