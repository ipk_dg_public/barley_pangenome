#!/bin/zsh

db=$1
query=$2
bed=$3
id=$4
length=$5

cmd="/opt/Bio/blast+/2.2.31/bin/legacy_blast.pl megablast --path /opt/Bio/blast+/2.2.31/bin/"
fasta=${bed:r}.fasta
err=${bed:r}.err
output=${bed:r}.megablast.tsv

{ 
 cut -f 1 $bed | xargs samtools faidx $query > $fasta && \
  { eval $cmd -m 8 -W 50 -M 1000000 -d $db -i $fasta \
   | awk '$3 >= '$id' && $4 >= '$length > $output 
  } && rm -f $fasta
} 2> $err
