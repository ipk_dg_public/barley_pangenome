Barley pan-genome analysis:

This repository holds scripts used in the analysis of the 
first-generation barley pan-genome.

* PAV_based_GWAS:
Source code to count k-mers extracted from single-copy regions residing 
within presence/absence variants (PAV) in raw sequencing reads (either 
GBS or WGS).

* PAV_based_PCA:
A script for principal component analysis (PCA) using k-mer counts from 
single-copy PAV regions. 

* Single_copy_pangenome:
Construction of single-copy pan-genome from chromosome-scale genome 
assemblies. Scripts to identify single-copy genomic regions and cluster 
them to make representative single-copy regions. Finally, construct 
single-copy pan-genome using representative single-copy regions from 
multiple genome assemblies.
